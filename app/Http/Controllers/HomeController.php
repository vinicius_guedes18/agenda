<?php

namespace App\Http\Controllers;

use App\Contato;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Contato::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
            ->get();
        $chart = Charts::database($users, 'bar', 'highcharts')
            ->title("Registro de contatos por mês")
            ->elementLabel("Total de contatos")
            ->responsive(true)
            ->groupByMonth(date('Y'), true);
        $pie = Charts::database($users, 'pie', 'highcharts')
            ->title("Registro de contatos por mês")
            ->elementLabel("Total de contatos")
            ->responsive(true)
            ->groupByMonth(date('Y'), true);
        return view('home', compact('chart', 'pie'));
    }
}
